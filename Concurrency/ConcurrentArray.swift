//
//  ConcurrentArray.swift
//  Obliterate
//
//  Created by Iain Henderson on 10/23/16.
//  Copyright © 2016 Iain Henderson. All rights reserved.
//

import Foundation

class ConcurrentArray<Element>: Collection{
    internal var elements: Array<Element>
    internal let queue:DispatchQueue = DispatchQueue(label: UUID.init().uuidString)

    init() {
        elements = Array<Element>()
    }
    
    convenience init<S : Sequence>(_ s: S) where S.Iterator.Element == Element{
        self.init()
        elements = Array(s)
    }
    convenience init(arrayLiteral elements: Element...){
        self.init()
        self.elements = Array(elements)
    }
    
    convenience init(repeating repeatedValue: Element, count: Int){
        self.init()
        elements = Array(repeating:repeatedValue, count:count)
    }

    var capacity: Int {
        get{
            let value: Int
            queue.sync {
                value = elements.capacity
            }
            return value
        }
    }
    var debugDescription: String {
        get{
            let value: String
            queue.sync {
                value = elements.debugDescription
            }
            return value
        }
    }

    var endIndex: Int{
        get{
            let value: Int
            queue.sync {
                value = elements.endIndex
            }
            return value
        }
    }

    var startIndex: Int{
        get{
            let value: Int
            queue.sync {
                value = elements.startIndex
            }
            return value
        }
    }

    var count: Int{
        get{
            let value: Int
            queue.sync {
                value = elements.count
            }
            return value
        }
    }

//    var customMirror: Mirror
 //   A mirror that reflects the array.
    var first: Element? {
        get{
            let value: Element?
            queue.sync {
                value = elements.first
            }
            return value
        }
    }

    var indices: CountableRange<Int>{
        get{
            let value: CountableRange<Int>
            queue.sync {
                value = elements.indices
            }
            return value
        }
    }

    var isEmpty: Bool{
        get{
            let value: Bool
            queue.sync {
                value = elements.isEmpty
            }
            return value
        }
    }

    var last: Element? {
        get{
            let value: Element?
            queue.sync {
                value = elements.last
            }
            return value
        }
    }

//    var lazy: LazyCollection<Array<Element>>
//    A view onto this collection that provides lazy implementations of normally eager operations, such as map and filter.
//    var lazy: LazySequence<Array<Element>>
//    A sequence containing the same elements as this sequence, but on which some operations, such as map and filter, are implemented lazily.
//    var lazy: LazyBidirectionalCollection<Array<Element>>
//    A view onto this collection that provides lazy implementations of normally eager operations, such as map and filter.
//    var lazy: LazyRandomAccessCollection<Array<Element>>
//    A view onto this collection that provides lazy implementations of normally eager operations, such as map and filter.
    var underestimatedCount: Int {
        get{
            let value: Int
            queue.sync {
                value = elements.underestimatedCount
            }
            return value
        }
    }

//    Returns a value less than or equal to the number of elements in the sequence, nondestructively.
    func append(_ element:Element) {
        queue.sync {
            elements.append(element)
        }
    }
    func popLast() -> Element? {
        var value: Element?
        queue.sync {
            value = elements.popLast()
        }
        return value
    }
    func reserveCapacity(_ minimumCapacity: Int){
        queue.sync {
            elements.reserveCapacity(minimumCapacity)
        }
    }
/*
    func withUnsafeBufferPointer<R>((UnsafeBufferPointer<Element>) -> R)
    Calls a closure with a pointer to the array’s contiguous storage. If no such storage exists, it is first created.
    func withUnsafeMutableBufferPointer<R>((inout UnsafeMutableBufferPointer<Element>) -> R)
    Calls the given closure with a pointer to the array’s mutable contiguous storage. If no such storage exists, it is first created.
*/
    func append<C: Collection>(contentsOf: C){
        queue.sync {
            elements.append(contentsOf: C)
        }
    }
    func append<S:Sequence>(contentsOf: S){
        queue.sync {
            elements.append(contentsOf: S)
        }
    }
    func distance(from: Int, to: Int) -> Int{
        var value: Int
        queue.sync {
            value = elements.distance(from:from, to:to)
        }
        return value
    }
//    Returns the distance between two indices.
    func dropFirst() -> ArraySlice<Element>{
        var shorty:ArraySlice<Element>
        queue.sync {
            shorty = elements.dropFirst()
        }
        return shorty
    }
//    Returns a subsequence containing all but the first element of the sequence.
    func dropFirst(_ trim:Int) -> ArraySlice<Element>{
        var shorty:ArraySlice<Element>
        queue.sync {
            shorty = elements.dropFirst(trim)
        }
        return shorty
    }

//    Returns a subsequence containing all but the given number of initial elements.
    func dropLast() -> ArraySlice<Element>{
        var shorty:ArraySlice<Element>
        queue.sync {
            shorty = elements.dropLast()
        }
        return shorty
    }

//    Returns a subsequence containing all but the last element of the sequence.
    func dropLast(_ trim:Int) -> ArraySlice<Element>{
        var shorty:ArraySlice<Element>
        queue.sync {
            shorty = elements.dropLast(trim)
        }
        return shorty
//    Returns a subsequence containing all but the specified number of final elements.
    func elementsEqual<OtherSequence: Sequence>(_ otherSequence:OtherSequence) -> Bool {
        var elementsEqual = false
        queue.sync {
            elements.elementsEqual(<#T##other: Sequence##Sequence#>, by: <#T##(Element, Element) throws -> Bool#>)
        }
        return elementsEqual
    }
//    Returns a Boolean value indicating whether this sequence and another sequence contain the same elements in the same order.
//    func enumerated()
//    Returns a sequence of pairs (n, x), where n represents a consecutive integer starting at zero, and x represents an element of the sequence.
//    func filter((Element) -> Bool)
//    Returns an array containing, in order, the elements of the sequence that satisfy the given predicate.
//    func first(where: (Element) -> Bool)
//    Returns the first element of the sequence that satisfies the given predicate or nil if no such element is found.
//    func flatMap<ElementOfResult>((Element) -> ElementOfResult?)
//    Returns an array containing the non-nil results of calling the given transformation with each element of this sequence.
//    func flatMap<SegmentOfResult>((Element) -> SegmentOfResult)
//    Returns an array containing the concatenated results of calling the given transformation with each element of this sequence.
//    func forEach((Element) -> Void)
//    Calls the given closure on each element in the sequence in the same order as a for-in loop.
//    func formIndex(inout Int, offsetBy: Int)
//    Offsets the given index by the specified distance.
//    func formIndex(inout Int, offsetBy: Int, limitedBy: Int)
//    Offsets the given index by the specified distance, or so that it equals the given limiting index.
//    func formIndex(after: inout Int)
//    func formIndex(before: inout Int)
//    func index(Int, offsetBy: Int)
//    Returns an index that is the specified distance from the given index.
//    func index(Int, offsetBy: Int, limitedBy: Int)
//    Returns an index that is the specified distance from the given index, unless that distance is beyond a given limiting index.
//    func index(after: Int)
//    func index(before: Int)
//    func index(of: Element)
//    Returns the first index where the specified value appears in the collection.
//    func index(where: (Element) -> Bool)
//    Returns the first index in which an element of the collection satisfies the given predicate.
//    func insert(Element, at: Int)
//    Inserts a new element at the specified position.
//    func joined(separator: String)
//    Returns a new string by concatenating the elements of the sequence, adding the given separator between each element.
//    func joined<Separator>(separator: Separator)
//    Returns the concatenated elements of this sequence of sequences, inserting the given separator between each element.
//    func lexicographicallyPrecedes<OtherSequence>(OtherSequence)
//    Returns a Boolean value indicating whether the sequence precedes another sequence in a lexicographical (dictionary) ordering, using the less-than operator (<) to compare elements.
//    func makeIterator()
//    Returns an iterator over the elements of the collection.
//    func map<T>((Element) -> T)
//    Returns an array containing the results of mapping the given closure over the sequence’s elements.
//    func map<T>((Element) -> T)
//    Returns an array containing the results of mapping the given closure over the sequence’s elements.
//    func max()
//    Returns the maximum element in the sequence.
//    func min()
//    Returns the minimum element in the sequence.
//    func prefix(Int)
//    Returns a subsequence, up to the specified maximum length, containing the initial elements of the collection.
//    func prefix(through: Int)
//    Returns a subsequence from the start of the collection through the specified position.
//    func prefix(upTo: Int)
//    Returns a subsequence from the start of the collection up to, but not including, the specified position.
//    func remove(at: Int)
//    Removes and returns the element at the specified position.
//    func removeAll(keepingCapacity: Bool)
//    Removes all elements from the array.
//    func replaceSubrange<C>(Range<Int>, with: C)
//    Replaces a range of elements with the elements in the specified collection.
//    func reversed()
//    Returns a view presenting the elements of the collection in reverse order.
//    func sort()
//    Sorts the collection in place.
//    func sorted()
//    Returns the elements of the collection, sorted.
//    func sorted()
//    Returns the elements of the sequence, sorted.
//    func split(separator: Element, maxSplits: Int, omittingEmptySubsequences: Bool)
//    Returns the longest possible subsequences of the collection, in order, around elements equal to the given element.
//    func split(separator: Element, maxSplits: Int, omittingEmptySubsequences: Bool)
//    Returns the longest possible subsequences of the sequence, in order, around elements equal to the given element.
//    func starts<PossiblePrefix>(with: PossiblePrefix)
//    Returns a Boolean value indicating whether the initial elements of the sequence are the same as the elements in another sequence.
//    func suffix(Int)
//    Returns a subsequence, up to the given maximum length, containing the final elements of the collection.
//    func suffix(from: Int)
//    Returns a subsequence from the specified position to the end of the collection.
//    func contains(where: (Element) -> Bool)
//    Returns a Boolean value indicating whether the sequence contains an element that satisfies the given predicate.
//    func elementsEqual<OtherSequence>(OtherSequence, by: (Element, Element) -> Bool)
//    Returns a Boolean value indicating whether this sequence and another sequence contain equivalent elements, using the given predicate as the equivalence test.
//    func lexicographicallyPrecedes<OtherSequence>(OtherSequence, by: (Element, Element) -> Bool)
//    Returns a Boolean value indicating whether the sequence precedes another sequence in a lexicographical (dictionary) ordering, using the given predicate to compare elements.
//    func max(by: (Element, Element) -> Bool)
//    Returns the maximum element in the sequence, using the given predicate as the comparison between elements.
//    func min(by: (Element, Element) -> Bool)
//    Returns the minimum element in the sequence, using the given predicate as the comparison between elements.
//    func partition(by: (Element) -> Bool)
//    Reorders the elements in the collection and returns a pivot index, using the given predicate as the comparison between elements.
//    func reduce<Result>(Result, (Result, Element) -> Result)
//    Returns the result of calling the given combining closure with each element of this sequence and an accumulating value.
//    func sort(by: (Element, Element) -> Bool)
//    Sorts the collection in place, using the given predicate as the comparison between elements.
//    func sorted(by: (Element, Element) -> Bool)
//    Returns the elements of the collection, sorted using the given predicate as the comparison between elements.
//    func split(maxSplits: Int, omittingEmptySubsequences: Bool, whereSeparator: (Element) -> Bool)
//    Returns the longest possible subsequences of the collection, in order, that don’t contain elements satisfying the given predicate.
//    func starts<PossiblePrefix>(with: PossiblePrefix, by: (Element, Element) -> Bool)
//    Returns a Boolean value indicating whether the initial elements of the sequence are equivalent to the elements in another sequence, using the given predicate as the equivalence test.
//    func joined()
//    Returns the elements of this sequence of sequences, concatenated.
//    func joined()
//    Returns the elements of this collection of collections, concatenated.
//    func joined()
//    Returns the elements of this collection of collections, concatenated.
//    func partition(by: (Element) -> Bool)
//    Subscripts
//    subscript(Int)
//    Accesses the element at the specified position.
//    subscript(Range<Int>)
//    Accesses a contiguous subrange of the array’s elements.
//    subscript(Range<Int>)
//    Accesses a contiguous subrange of the collection’s elements.
//    subscript(CountableRange<Int>)
//    Accesses a contiguous subrange of the collection’s elements.
//    subscript(ClosedRange<Int>)
//    Accesses a contiguous subrange of the collection’s elements.
//    subscript(CountableClosedRange<Int>)
//    Accesses a contiguous subrange of the collection’s elements.
//    subscript(CountableRange<Int>)
//    Accesses a contiguous subrange of the collection’s elements.
//    subscript(CountableClosedRange<Int>)
//    Accesses a contiguous subrange of the collection’s elements.
//    subscript(ClosedRange<Int>)
//    Accesses a contiguous subrange of the collection’s elements.
// */
}
