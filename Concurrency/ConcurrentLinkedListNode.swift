//
//  ConcurrentLinkedListNode.swift
//  Concurrency
//
//  Created by Iain Henderson on 10/22/16.
//  Copyright © 2016 Iain Henderson. All rights reserved.
//

class ConcurrentLinkedListNode<Element>{
    typealias Node<Element> = ConcurrentLinkedListNode<Element>
    
    internal var nextNode: Node<Element>?
    internal var previousNode: Node<Element>?
    internal var value: Element
    
    init(_ value: Element, previous: Node<Element>? = nil, next: Node<Element>? = nil){
        self.value = value
        if((previous) != nil){
            self.previousNode = previous
        }
        if((next) != nil){
            self.nextNode = next
        }
    }
    
    func get() -> Element {
        return value
    }
    
    func next(_ next: Node<Element>? = nil) -> Node<Element>?{
        return nextNode
    }
    
    func previous() -> Node<Element>?{
        return previousNode
    }
}
