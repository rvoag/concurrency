//
//  ConcurrentStack.swift
//  Obliterate
//
//  Created by Iain Henderson on 10/22/16.
//  Copyright © 2016 Iain Henderson. All rights reserved.
//

import Foundation

class ConcurrentStack<Element> : ConcurrentArray<Element> {
    func push(_ next: Element){
        queue.sync {
            elements.append(next);
        }
    }
    
    func pop() -> Element? {
        var last: Element? = nil
        queue.sync {
            if(!elements.isEmpty){
                last = elements.removeLast();
            }
        }
        return last;
    }
}
