//
//  ConcurrentDeque.swift
//  Obliterate
//
//  Created by Iain Henderson on 10/22/16.
//  Copyright © 2016 Iain Henderson. All rights reserved.
//

import Foundation
class ConcurrentQueue<Equatable> : ConcurrentCollection<Equatable> {
    func push(_ next: Equatable){
        queue.sync {
            Equatables.append(next);
        }
    }
    
    func poll() -> Equatable? {
        var first: Equatable? = nil
        queue.sync {
            if(!Equatables.isEmpty){
                first = Equatables.removeLast();
            }
        }
        return first;
    }
    
}
