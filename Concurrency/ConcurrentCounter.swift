//
//  ConcurrentCounter.swift
//  Obliterate
//
//  Created by Iain Henderson on 10/22/16.
//  Copyright © 2016 Iain Henderson. All rights reserved.
//

import Foundation

class ConcurrentCounter {
    private var queue = DispatchQueue(label: UUID.init().uuidString)
    private (set) var value: Int = 0
    
    func decrement(){
        queue.sync {
            value -= 1
        }
    }
    
    func decrementAndGet() -> Int{
        var current = 0
        queue.sync {
            value -= 1
            current = value
        }
        return current
    }
    
    func get() -> Int {
        var current = 0
        queue.sync {
            current = value
        }
        return current
    }
    
    func getAndDecrement() -> Int {
        var current = 0
        queue.sync {
            current = value
            value -= 1
        }
        return current
    }
    
    func getAndIncrement() -> Int {
        var current = 0
        queue.sync {
            current = value
            value += 1
        }
        return current
    }
    
    func increment(){
        queue.sync {
            value += 1
        }
    }
    
    func incrementAndGet() -> Int {
        var current = 0
        queue.sync {
            value += 1
            current = value
        }
        return current
    }
    
}
