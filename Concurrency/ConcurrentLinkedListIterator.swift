//
//  ConcurrentLinkedListIterator.swift
//  Obliterate
//
//  Created by Iain Henderson on 10/22/16.
//  Copyright © 2016 Iain Henderson. All rights reserved.
//

import Foundation

class ConcurrentLinkedListIterator<Element>: IteratorProtocol {
    typealias Node<Element> = ConcurrentLinkedListNode<Element>
    
    internal var current:Node<Element>?
    internal let queue:DispatchQueue
    
    init(_ source:ConcurrentLinkedListNode<Element>, _ queue:DispatchQueue){
        self.current = source
        self.queue = queue
    }
    
    func next() -> Element? {
        var value:Element? = nil
        if((current) == nil){
            queue.sync {
                value = current!.get()
                current = current!.next()
            }
        }
        return value
    }
}
