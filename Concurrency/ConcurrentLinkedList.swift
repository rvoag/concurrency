//
//  ConcurrentLinkedList.swift
//  Obliterate
//
//  Created by Iain Henderson on 10/22/16.
//  Copyright © 2016 Iain Henderson. All rights reserved.
//

import Foundation

class ConcurrentLinkedList<Element> : Sequence{
    typealias Node<Element> = ConcurrentLinkedListNode<Element>

    internal let queue:DispatchQueue
    
    internal var first: Node<Element>?
    internal var last: Node<Element>?
    internal let size:ConcurrentCounter
    
    init(_ value:Element? = nil){
        queue = DispatchQueue(label: UUID.init().uuidString)
        size = ConcurrentCounter();
        if((value) != nil){
            addLast(value!)
        }
    }
    
    func append(_ value:Element){
        addLast(value)
    }
    
    func add(_ value:Element){
        addLast(value)
    }
    
    func addFirst(_ value:Element){
        queue.sync {
            first = Node(value, next: first)
            if(size.getAndIncrement() == 0) {
                last = first
            }
        }
    }
    
    func addLast(_ value:Element){
        queue.sync {
            last = Node(value, previous: last)
            if(size.getAndIncrement() == 0) {
                first = last
            }
        }
    }
    
    func get() -> Element?{
        var value:Element? = nil
        queue.sync {
            if((first) != nil){
                value = first!.get()
            }
        }
        return value
    }
    
    func getFirst() -> Element?{
        var value:Element? = nil
        queue.sync {
            if((first) != nil){
                value = first!.get()
            }
        }
        return value
    }
    
    func getLast() -> Element?{
        var value:Element? = nil
        queue.sync {
            if((last) != nil){
                value = last!.get()
            }
        }
        return value
    }
    
    func isEmpty() -> Bool{
        return (first) == nil
    }
    
    func removeFirst() -> Element? {
        var value:Element?
        queue.sync {
            if((first) != nil){
                value = first!.get();
                first = first?.next();
                if(size.decrementAndGet() == 0) {
                    last = nil
                }
            }
        }
        return value
    }
    
    func removeLast() -> Element? {
        var value:Element?
        queue.sync {
            if((first) != nil){
                value = first!.get();
                last = last?.previous();
                if(size.decrementAndGet() == 0) {
                    first = nil
                }
            }
        }
        return value
    }
    
    func makeIterator() -> ConcurrentLinkedListIterator<Element> {
        return ConcurrentLinkedListIterator(first, queue)
    }
}

